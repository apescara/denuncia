/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denunciacliente;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author andre
 */
public class DenunciaCliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        final String ubicacion;
        String descripcion;
        int prioridad;
        int tipo;
        String serverName = args[0];
        int port = Integer.parseInt(args[1]);
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Conectando a " + serverName + " en puerto " + port);
            Socket client = new Socket(serverName, port);
            System.out.println("Conectado a  " + client.getRemoteSocketAddress());
            OutputStream outToServer = client.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToServer);
            System.out.println("Ingrese el numero de la denuncia que quiere registrar:");
            System.out.println("1.- Robo");
            System.out.println("2.- Rayado");
            System.out.println("3.- Mascota perdida");
            System.out.println("4.- Servicio en mal estado");
            System.out.println("5.- Ruidos molestos");
            tipo = scanner.nextInt();
            System.out.println("Ingrese una descripcion de la denuncia");
            descripcion = scanner.nextLine();
            System.out.println("Ingrese la ubicacion de la denuncia");
            ubicacion = scanner.nextLine(); 
            System.out.println("Ingrese la prioridad (de 1 a 5):");
            prioridad = scanner.nextInt();
            out.writeUTF(tipo + "|" + descripcion + "|" + ubicacion + "|" + prioridad + "|");//datos a enviar
            InputStream inFromServer = client.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);
            System.out.println("Servidor: " + in.readUTF());
            client.close();
        }catch(IOException e) {
                e.printStackTrace();
        }
    }
    
}
